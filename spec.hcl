context Consultation(patient, doctor, hospital) {
  commitment StoreData(hospital->patient):
    created: patient.Visit{date}
    detached: doctor.Record{patient, item}
    discharged: hospital.Store{item}

  commitment DestroyData(hospital->patient):
    created: hospital.Store{item}
    detached: patient.RequestDeletion{item}
    discharged: hospital.Delete{item}

  authorization Access(patient->recipient, item):
    created: patient.GrantAccess{recipient, item} @ t
    detached: recipient.RequestAccess{item} @ t2 > t
              except patient.RevokeAccess{recipient, item} @ [t, t2]
    discharged: hospital.Shared{item, recipient} @ [t2, t2+10]

  prohibition Disclosure(patient->hospital):
    created: hospital.Store{patient, item}
    violated: hospital.Shared{item, recipient}
              except Access(patient->recipient, item):detached
}