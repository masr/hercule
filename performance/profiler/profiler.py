#
# This file hacked together from https://github.com/excieve/dragnet
#

import logging
import argparse
import json
import importlib.util
import os.path
import sys
import pandas
import subprocess
import time
from requests_futures.sessions import FuturesSession
from cloudant import couchdb

logger = logging.getLogger('dragnet.profile')


def load_source(mapfile_name, reducefile_name=None):
    map_function_source = None
    with open(mapfile_name, 'r', encoding='utf-8') as f:
        map_function_source = f.read()
    logger.info('Loaded map function, size: {}'.format(
        len(map_function_source)))

    reduce_function_source = None
    if reducefile_name:
        with open(reducefile_name, 'r', encoding='utf-8') as f:
            reduce_function_source = f.read()
        logger.info('Loaded reduce function, size: {}'.format(
            len(map_function_source)))

    return map_function_source, reduce_function_source


def execute_view(couch, view):
    # Call the view in order to launch the indexing process.
    # This is fire and forget as it will most likely timeout and we're not interested in the result anyway.
    session = FuturesSession()
    session.get('{}?limit=1&reduce=false'.format(view.url))
    logger.info('Initiated the view query')
    time.sleep(2)

    # Query CouchDB active tasks for the view's design doc while there's nothing left, then calculate average
    # Taken and adapted from https://gist.github.com/kevinjqiu/dd461b36a6f1d6d755d7a317d8f98b75#file-cps-py
    all_changes_per_sec = []
    all_changes_per_sec_one_task = []
    total_time = 0
    logger.info('Querying active tasks for the design document.')
    while True:
        response = couch.r_session.get(
            '{}/_active_tasks'.format(couch.server_url))
        logger.debug(response.text)
        tasks = [
            task for task in response.json()
            if task.get('design_document') == view.design_doc['_id']
        ]
        if len(tasks) == 0:
            break

        task = tasks[0]
        started_on, updated_on = task['started_on'], task['updated_on']
        # This will typically end up to be the last task's time, thus longest
        total_time = updated_on - started_on
        if total_time == 0:
            continue

        changes_per_sec = []
        for t in tasks:
            diff_t = float(t['updated_on'] - t['started_on'])
            if diff_t > 0:
                changes_per_sec.append(
                    float(t.get('changes_done', 0)) / diff_t)
        all_changes_per_sec.append(sum(changes_per_sec))
        all_changes_per_sec_one_task.append(changes_per_sec[0])
        logger.info('c/s = {:.2f} ({} tasks), {:.2f} (one task); changes = {}'
                    .format(
                        sum(changes_per_sec), len(tasks),
                        changes_per_sec[0],
                        sum([t.get('changes_done', 0) for t in tasks])
                    ))
        time.sleep(1)

    if all_changes_per_sec:
        average_all = sum(all_changes_per_sec) / len(all_changes_per_sec)
        average_all_one_task = sum(
            all_changes_per_sec_one_task) / len(all_changes_per_sec_one_task)
        logger.info('average = {:.2f}c/s (all_tasks) {:.2f}c/s (one task)'.format(
            average_all, average_all_one_task))
        logger.info('total time = {}s'.format(total_time))
    else:
        logger.info('No active tasks (not indexing).')
    return all_changes_per_sec


def add_function(map_function_source, reduce_function_source, db_config, language, execute):
    with couchdb(db_config['user'], db_config['password'], url=db_config['url']) as couch:
        db = couch[db_config['name']]
        design_doc = db.get_design_document(db_config['design_doc'])
        logger.info('Working with design document {} from DB {}.'.format(
            design_doc['_id'], db.database_name))
        design_doc['language'] = language
        view = design_doc.get_view(db_config['view'])
        if view is None:
            logger.info('View {} does not exist, creating a new one.'.format(
                db_config['view']))
            design_doc.add_view(
                db_config['view'], map_function_source, reduce_function_source)
        else:
            logger.info('View {} exists, updating.'.format(view.view_name))
            design_doc.update_view(
                db_config['view'], map_function_source, reduce_function_source)
        logger.debug(design_doc.json())
        design_doc.save()
        logger.info('Design doc successfully saved.')
        logger.debug(design_doc.info())
        if execute:
            view = design_doc.get_view(db_config['view'])
            return execute_view(couch, view)


def runner(profile, db_config):
    runner_profile = profile['runner']
    assert runner_profile['type'] in [
        'chakra', 'javascript'], 'Unsupported runner type'

    logger.info('Executing view runners...')
    exec_stats = []
    for design_doc in runner_profile['design_documents']:
        logger.info('Processing design document "{}":'.format(
            design_doc['name']))
        for view in design_doc['views']:
            view_name = os.path.splitext(os.path.basename(view))[0]
            logger.info('Loading and executing view "{}"...'.format(view_name))
            map_function_source, _ = load_source(view)
            func_db_config = {
                'design_doc': design_doc['name'],
                'view': view_name
            }
            func_db_config.update(db_config)
            exec_stat = add_function(map_function_source, reduce_function_source=None, db_config=func_db_config,
                                     language=runner_profile['type'], execute=runner_profile['sequential'])
            exec_stats.append(exec_stat)
    return exec_stats


if __name__ == '__main__':
    logging.basicConfig()
    logging.getLogger().setLevel(logging.INFO)

    parser = argparse.ArgumentParser(description='Execute a Dragnet profile')
    parser.add_argument('action',
                        help='Action to execute (use "all" to sequentially perform every action except "loader")',
                        choices=['all', 'runner'])
    parser.add_argument('profile', help='Profile to execute')
    parser.add_argument('-d', '--datadir',
                        help='Data directory', default='data')
    parser.add_argument('-u', '--username',
                        help='CouchDB username', default='admin')
    parser.add_argument('-p', '--password',
                        help='CouchDB password', default='password')
    parser.add_argument(
        '-e', '--endpoint', help='CouchDB endpoint', default='http://couchdb:5984')
    parser.add_argument('-E', '--elasticsearch',
                        help='ElasticSearch endpoint', default='http://localhost:9200')
    parser.add_argument('-n', '--noreexport', help='Take no further action if no execution was detected',
                        action='store_true', default=False)
    parser.add_argument('-c', '--concurrency',
                        help='Number of processes to spawn', type=int, default=8)
    parser.add_argument(
        '-C', '--chunks', help='Number of chunks to run in a batch', type=int, default=200)
    args = parser.parse_args()

    with open('{}/profiles/{}.json'.format(args.datadir, args.profile), 'r', encoding='utf-8') as fp:
        raw_json = fp.read()
        profile = json.loads(raw_json.replace('{datadir}', args.datadir))

    db_config = {
        'user': args.username,
        'password': args.password,
        'url': args.endpoint,
        'name': profile['database']
    }

    logger.info('Profile "{}" successfully loaded.'.format(profile['name']))

    if args.action == 'loader' and 'loader' in profile:
        loader(profile, db_config, args.concurrency, args.chunks)
    if args.action in ('all', 'runner') and 'runner' in profile:
        exec_stats = runner(profile, db_config)
    if args.noreexport and not any(exec_stats):
        logger.info(
            'No runner execution detected and "noreexport" is set -- preventing further actions.')
        sys.exit(0)

    logger.info('Profiles execution complete.')
