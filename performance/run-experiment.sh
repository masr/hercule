#!/bin/bash

NUM_ENACTMENTS=${1:-200000}
WAIT=${2:-60}
HOST=${HOST:-127.0.0.1}

docker-compose down

# create view files
node ../node/cli compile ../spec.hcl profiler/views

docker-compose build
docker-compose up -d couchdb

sleep 2

DB="http://admin:password@${HOST}:5984"

curl -X PUT "${DB}/_users"
curl -X PUT "${DB}/_replicator"
curl -X PUT "${DB}/_global_changes"
curl -X PUT "${DB}/hercule-test"

node ../node/cli generate ${NUM_ENACTMENTS} enactments -d "${DB}/hercule-test" 2>/dev/null

echo "Waiting for documents to finish loading..."

sleep ${WAIT}

docker-compose run profiler >results

echo "Performance test complete. See 'results' file for captured data."
