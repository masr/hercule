var acorn = require('acorn')
var astring = require('astring')
var pegjs = require('pegjs')
var Promise = require('bluebird');
var fs = Promise.promisifyAll(require('fs'));
var _ = require('lodash')

var ast = require('./ast')
var view = require('./view-compiler')

function loadGrammar(options, path) {
  return fs.readFileAsync(path || (__dirname + '/grammar.pegjs'), 'utf8')
    .then(g => pegjs.generate(g, {allowedStartRules: ['Context', 'EventConjunction']}))
    .then(function(obj) {
      return obj.parse
    })
}

function parse(data, options) {
  return loadGrammar(options).then(p => p(data, options))
}

function parseRule(data, rule) {
  return parse(data, {allowedStartRules: [rule], startRule: rule})
}

function makeViews(context) {
  let c = view.processContext(context, ast.identifier('doc'))
  return _.mapValues(c.norms, function(norm, normName) {
    return _.mapValues(norm, function(state, stateName) {
      try {
        return {map: astring.generate(
          ast.func([ast.identifier('doc')],
                   [ast.and(state, ast.call(ast.identifier('emit'),
                                            [ast.identifier('doc')]))]))}
      } catch (e) {
        console.log(normName, stateName)
        console.error(e)
        console.log(JSON.stringify(state))
        var types = ast.traverse(state, function(pending, accum) {
          curr = pending.pop()
          switch (curr.type) {
          case "Norm":
            pending = pending.concat(_.reverse(curr.states)) //push them all on like a stack
            break
          case "State":
            pending.push(curr.expression)
            break
          case "UnaryExpression":
            pending.push(curr.argument)
            break
          default:
            if (curr.left) {
              //console.log(curr.left, curr.right)
              pending.push(curr.right)
              pending.push(curr.left) //push left second so that it pops first
            }
            break
          }
          accum.push(curr.type)
          return [pending, accum]
        })
        console.log(_.uniq(types))
      }
    })
  })
}

function designDocs(context) {
  let views = makeViews(context)
  return _.mapValues(views, v => {
    return {
      language: 'javascript',
      views: v
    }
  })
}

function compile(path) {
  var name
  return fs.readFileAsync(path, 'utf-8')
    .then(parse)
    .then(function(context) {
      name = context.name;
      return context
    })
    .then(designDocs)
    .catch(console.error)
}

function loadSpec(path, db) {
  var name
  return compile(path)
    .then(docs => {
      _.keys(docs).forEach(function(k) {
        Promise.resolve(
          db.getDesignDocument(db.name, k).then(function({data}) {
            return db.deleteDesignDocument(db.name, k, data._rev).catch(console.error)
          })).finally(function() {
            return db.createDesignDocument(db.name, docs[k], k).catch(console.error)
          })
      })
      return docs
    }).catch(console.error)
}

function startEnacment(db, context, enactment) {
  context.roles.forEach(r => {
    if (!enactment.roles[r]) {
      throw("Role " + r + " not bound in enactment: " + JSON.stringify(enactment))
    }
  })
  return db.createDocument(db.name, enactment)
}

function addEvent(event, enactment) {
  enactment[event.name] = event
  enactment.merge
}

  // db.getDocument(db.name, event.context)
  //   .then(({data: doc}) => {
  //     if (doc[type]) {
  //       doc[type].push(event)
  //     } else {
  //       doc[type] = [event]
  //     }
  //     return db.createDocument(db.name, doc, event.context)
  //   })

function lib(fn) {
  return "module.exports = " + fn.toString()
}

function view(path) {
  return {
    map: "function (doc) { return require('" + path + "')(doc).map(emit) }"
  }
}

module.exports = {
  parse,
  parseRule,
  makeViews,
  designDocs,
  loadSpec,
  startEnacment,
  compile
}
