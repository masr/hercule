#!/usr/bin/env node

const {
  parse,
  parseRule,
  makeViews,
  designDocs,
  loadSpec,
  compile
} = require('./hercule');
var _ = require('lodash');

var Promise = require('bluebird');
var fs = Promise.promisifyAll(require('fs'));
var path = require('path')

const program = require('commander');

program
  .version('0.0.1')

function connect(url,name) {
  if (match = url.match(/(.*\/\/.*)\/(.*)$/)) {
    url = match[1]
    var dbName = match[2]
  }
  dbName = dbName || name || "hercule-test"
  db = require('couchdb-promises')({
    baseUrl: url,
    requestTimeout:10000
  })
  db.name = dbName
  db.listDatabases().then(function(dbList) {
    if (!dbList.data.includes(dbName)) {
      console.log("Creating database: ", dbName)
      db.createDatabase(dbName).catch(console.error)
    }
  }).catch(console.error)
  return db
}

async function saveViewData(dbName,path,ddocs=['Access','DestroyData']){
  var data={};
  for (ddoc of ddocs){
      var ddocs_info = await db.getDesignDocument(dbName,ddoc);
      for (var view in ddocs_info.data.views){
          view_data = await db.getView(dbName,ddoc,view);
          data[ddoc+'_'+view]=view_data.data.rows;
      }
  }
  fs.writeFileAsync(__dirname+'/' +path, JSON.stringify(data, null, 2));
}


program
  .command('load <file>')
  .description('Load a specification into the database')
  .action(function(file) {
    loadSpec(file, db)
  })

program
  .command('compile <file> <out-dir>')
  .description('Compile a specification file into design documents')
  .action(function(file, dir) {
    compile(file, dir).then(docs => {
      _.keys(docs).forEach(function(doc) {
        fs.promises.mkdir(path.join(dir, _.kebabCase(doc)), {recursive: true}).then(() => {
          _.keys(docs[doc].views).map(function(state) {
            fs.writeFileAsync(path.join(dir, _.kebabCase(doc), state+".js"),
                              docs[doc].views[state].map)
          })
        }).catch(console.error)
      })
    })
  })

program
  .command('enactment <contextName> <data>')
  .description('Start a new enactment in a given context')
  .action(function(contextName, data) {
    data = JSON.parse(data)
  })

program
  .command('event <type> <data>')
  .description('Record an event')
  .action(function(type, data) {
    data = JSON.parse(data)
  })

program
  .command('generate <number> <type>')
  .description('Generate events or specifications')
  .option('-r, --max-roles <num>', 'Maximum number of roles to choose from', 2)
  .option('-e, --max-event-names <num>', 'Maximum number of event names to choose from', 3)
  .option('-p, --max-params <num>', 'Maximum number of parameters to choose from', 2)
  .option('-d, --database [name]', 'Insert documents into database')
  .action(function(number, type, options) {
    if (number < 0) { console.error("Error: number of items to generate must be positive.") }
    if (! type.match(/events?|spec(ification|s)?|contexts?|enactments?/i)) {
      console.error("Error: Only events, enactments or specifications can be generated.")
    }
    if (options.database) {
      var db = connect(options.database)
    }

    const child_process = require("child_process");
    let generator = require('./generator')
    let sources = generator.generateSources(_.pick(options, ['maxRoles', 'maxEventNames', 'maxParams']))

    if (type.match(/events?/i))
      for (var i=0; i<number; i++)
        console.log(generator.generateEvent(sources))

    if (type.match(/spec(ification|s)?|contexts?/i))
      for (var i=0; i<number; i++) {
        let context = generator.generateContext(sources)
        context.name += i;
        console.log(JSON.stringify(context))
      }

    if (type.match(/enactments?/i)) {
      let enactments = []
      for (var i=0; i<number; i++) {
        let enactment = generator.generateEnactment()
        if (db) {
          enactments.push(enactment)
          if (i%1000==0) {
            console.log('\r'+enactments.length, i)
            db.createBulkDocuments(db.name, enactments).catch(console.error)
            //child_process.execSync("busybox usleep 250000");
            enactments = []
          }
        } else {
          console.log(JSON.stringify(enactment))
        }
      }
      if (db && enactments.length > 0) {
        console.log(enactments.length)
        db.createBulkDocuments(db.name, enactments).catch(console.error)
      }
    }
  })


program
  .command('dumpview <path>')
  .description("save the views to a json doc in filesystem")
  .action(function(path) {
    saveViewData(dbName,path)
  })
program.parse(process.argv); // end with parse to parse through the input0
