var assert = require('chai').assert
var should = require('chai').should()
var expect = require('chai').expect
var rewire = require('rewire')
var log = console.log
var _ = require('lodash')

var hercule = require('../hercule')
var ast = rewire('../ast')
var generator = rewire('../generator.js')

describe('generator', function() {
  var generateSources = generator.__get__('generateSources')
  let options = function () {return generateSources({maxRoles: 2, maxEventNames: 3, maxParams: 2})}

  describe('predefinedSources', function() {
    let predefinedSources = generator.__get__('predefinedSources')
    it('should support default parameters', function() {
      let sources = predefinedSources()
      sources.should.include.keys(['roles', 'eventNames', 'params'])
    })
    it('should select a number of predefined names', function() {
      let sources = predefinedSources()
      sources.should.include.keys(['roles', 'eventNames', 'params'])
      sources.roles.should.have.lengthOf(2)
      sources.eventNames.should.have.lengthOf(3)
      sources.params.should.have.lengthOf(2)
    })
  })

  describe('generateNames', function() {
    var generateNames = generator.__get__("generateNames")
    it('should generate sequential names with a given prefix', function() {
      generateNames("test", 3).should.eql(["test1", "test2", "test3"])
    })
  })

  describe('generateSources', function() {
    it('should generate some of each type of name', function() {
      let sources = generateSources()
      sources.should.have.all.keys(["roles", "eventNames", "params", "events"])
      sources.roles.length.should.be.above(1)
      sources.eventNames.length.should.be.above(1)
      sources.params.length.should.be.above(1)
    })
  })

  describe('generateEvent', function() {
    var generateEvent = generator.__get__('generateEvent')
    it('should generate a single event, given sources and a max number of params', function() {
      generateEvent(options()).should.have.all.keys(["type", "by", "name", "params"])
    })
    it('should work without a maxParams value', function() {
      generateEvent(_.omit(options(), ['maxParams'])).should.have.all.keys(["type", "by", "name", "params"])
    })
  })

  describe('generateExpressionTree', function() {
    var generateExpressionTree = generator.__get__('generateExpressionTree')
    it('should exist...', function() {
      expect(generateExpressionTree).to.exist
    })
    it('should return a generated expression tree', function() {
      let result = generateExpressionTree({...options(), })
      expect(result).to.exist
    })

    it('should have a BinaryExpression root with non-zero probability', function() {
      var exprs = _.times(10, () => generateExpressionTree({...options(), maxDepth:2}))
      _.some(exprs,
             t => t.type == 'BinaryExpression')
        .should.be.true
    })
  })

  describe('generateNorm', function() {
    var generateNorm = generator.__get__('generateNorm')
    it('should produce a norm object', function() {
      expect(generateNorm(options())).to.exist
    })

    it('should produce a norm with type and kind', function() {
      generateNorm(options()).should.include.keys(['type', 'kind'])
    })

    it('should have a created expression', function() {
      let norm = generateNorm(options())
      norm.should.include.keys(['type', 'created'])
      norm.created.should.include.keys(['type'])
      norm.created.type.should.be.oneOf(['BinaryExpression', 'UnaryExpression', 'Event'])
    })

    it('should have detached, discharged, and violated events with non-zero probability', function() {
      var norms = _.times(100, () => generateNorm({...options(), maxDepth:2}))
      _.some(norms, n => 'detached' in n).should.be.true
      _.some(norms, n => 'discharged' in n).should.be.true
      _.some(norms, n => 'violated' in n).should.be.true
    })

    it('should not produce a norm with both discharged and violated states', function() {
      var norms = _.times(10, () => generateNorm({...options(), maxDepth:2}))
      _.every(norms, n => !('discharged' in n && 'violated' in n)).should.be.true
    })
  })

  describe('generateContext', function() {
    var generateContext = generator.__get__('generateContext')
    it('should exist...', function() {
      expect(generateContext).to.exist
    })

    it('should return a generated context', function() {
      let result = generateContext()
      expect(result).to.exist
    })
  })

  describe('getContextEvents', function() {
    var generateContext = generator.__get__('generateContext')
    var getContextEvents = generator.__get__('getContextEvents')
    it('should get a list of events from a generated context', function() {
      let c = generateContext(options())
      let events = getContextEvents(c)
      expect(events).to.exist
      events.length.should.be.at.least(0)
    })
  })
})
