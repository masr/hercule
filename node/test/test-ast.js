var assert = require('chai').assert
var should = require('chai').should()
var astring = require('astring')
var rewire = require('rewire')
var log = console.log

var hercule = require('../hercule')
var ast = rewire('../ast')

describe('ast', function() {
  describe('stepEvents', function() {
    it('should return a single event', function() {
      ast.stepEvents([{type:'Event'}], []).should.eql([[], [{type:'Event'}]])
    })

    it('should traverse a UnaryExpression', function() {
      ast.stepEvents([{type: 'UnaryExpression', argument: {type:'Event'}}], [])
        .should.eql([[{type:'Event'}], []])
    })

    it('should traverse a BinaryExpression, left first', function() {
      ast.stepEvents([{type: 'BinaryExpression',
                 left: {type:'Event', name:'A'},
                 right: {type:'Event', name:'B'}}], [])
        .should.eql([[{type:'Event', name:'B'}, {type:'Event', name:'A'}], []])
    })
  })

  describe('extractEvents', function() {
    it('should return a single event', function() {
      ast.extractEvents({type:'Event'}).should.eql([{type:'Event'}])
    })

    it('should traverse a UnaryExpression, returning the event', function() {
      ast.extractEvents({type: 'UnaryExpression', argument: {type:'Event'}})
        .should.eql([{type:'Event'}])
    })

    it('should traverse a BinaryExpression, left first', function() {
      ast.extractEvents({type: 'BinaryExpression',
                         left: {type:'Event', name:'A'},
                         right: {type:'Event', name:'B'}})
        .should.eql([{type:'Event', name:'A'}, {type:'Event', name:'B'}])
    })

    it('should traverse a simple tree, returning all events in order', function() {
      ast.extractEvents({type: 'BinaryExpression',
                         left: {type: 'BinaryExpression',
                                left: {type:'Event', name:'A'},
                                right: {type:'Event', name:'B'}},
                         right: {type:'Event', name:'C'}})
        .should.eql([{type:'Event', name:'A'}, {type:'Event', name:'B'}, {type:'Event', name:'C'}])
    })
  })

  describe('extractTimeVars', function() {
    it('should return a single var', function() {
      ast.extractTimeVars({at: ast.identifier('t')}).should.eql(['t'])
    })

    it('should traverse a range, start first', function() {
      ast.extractTimeVars({start: ast.identifier('t'), end: ast.identifier('t2')})
        .should.eql(['t', 't2'])
    })

    it('should traverse a binary expression, left first', function() {
      ast.extractTimeVars({at: {type: 'BinaryExpression', operator: '>', left: ast.identifier('t2'), right: ast.identifier('t')}})
        .should.eql(['t2', 't'])
    })

    it('should traverse simple tree in order', function() {
      ast.extractTimeVars({start: ast.identifier('t'), end: {type: 'BinaryExpression', operator: '+', left: ast.identifier('t2'), right: ast.literal(10)}})
        .should.eql(['t', 't2'])
    })
  })


  describe('ast.identifier', function() {
    it('should make an identifier AST node', function() {
      ast.identifier('test').should.eql({type:'Identifier', name: 'test'})
    })
  })

  describe('ast.literal', function() {
    it('should make a Literal AST node', function() {
      ast.literal(10).should.eql({type:'Literal', value: 10})
    })
  })

  describe('ast.and', function() {
    it('should make an AST node representing a conjunction of two expressions', function() {
      ast.and(ast.literal(true), ast.literal(false))
        .should.eql({
          type:'LogicalExpression',
          operator: "&&",
          left: ast.literal(true),
          right: ast.literal(false)
        })
    })

    it('should make an AST node representing a conjunction of more than two expressions', function() {
      ast.and(ast.literal(true), ast.literal(false), ast.literal(true))
        .should.eql({
          type:'LogicalExpression',
          operator: "&&",
          left: ast.literal(true),
          right: {
            type:'LogicalExpression',
            operator: "&&",
            left: ast.literal(false),
            right: ast.literal(true)
          }
        })
    })

    it('"true && false && true" should eval to false', function() {
      var program = astring.generate(
        ast.and(ast.literal(true), ast.literal(false), ast.literal(true)))
      eval(program).should.equal(false)
    })

    it('"true && true && true" should eval to true', function() {
      var program = astring.generate(
        ast.and(ast.literal(true), ast.literal(true), ast.literal(true)))
      eval(program).should.equal(true)
    })
  })

  describe('ast.and', function() {
    it('should make an AST node representing a disjunction of two expressions', function() {
      ast.or(ast.literal(true), ast.literal(false))
        .should.eql({
          type:'LogicalExpression',
          operator: "||",
          left: ast.literal(true),
          right: ast.literal(false)
        })
    })

    it('should handle more than two expressions', function() {
      ast.or(ast.literal(true), ast.literal(false), ast.literal(true))
        .should.eql({
          type:'LogicalExpression',
          operator: "||",
          left: ast.literal(true),
          right: {
            type:'LogicalExpression',
            operator: "||",
            left: ast.literal(false),
            right: ast.literal(true)
          }
        })
    })

    it('"true || false || true" should eval to true', function() {
      var program = astring.generate(
        ast.or(ast.literal(true), ast.literal(false), ast.literal(true)))
      eval(program).should.equal(true)
    })

    it('"false || false || false" should eval to false', function() {
      var program = astring.generate(
        ast.or(ast.literal(false), ast.literal(false), ast.literal(false)))
      eval(program).should.equal(false)
    })
  })

  describe('equal', function() {
    it('equal(a,b) should compile to "a == b"', function() {
      var program = astring.generate(
        ast.equal(ast.literal(1), ast.literal(1))
      )
      eval(program).should.equal(true)
    })
  })

  describe('staticMember', function() {
    it('should lookup a property on an object', function() {
      let a = {b: 1}
      var program = astring.generate(
        ast.staticMember(ast.identifier('a'), ast.identifier('b'))
      )
      eval(program).should.equal(1)
    })
  })

  describe('filter', function() {
    it('should filter an array by an expression', function() {
      var program = astring.generate(
        ast.filter(ast.literal([1, 2, 3, 4]),
                   ast.func([ast.identifier('i')],
                            [ast.ret(ast.equal(ast.identifier('i'), ast.literal(2)))]))
      )
      eval(program).should.eql([2])
    })
  })
})
