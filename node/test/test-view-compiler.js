var assert = require('chai').assert
var should = require('chai').should()
var astring = require('astring')
var rewire = require('rewire')
var log = console.log

var hercule = require('../hercule')
var ast = rewire('../ast')
var view = rewire('../view-compiler')

describe('view-compiler', function() {
  describe('checkParamsExist', function() {
    it('should produce a conjuction of all the parameters', function() {
      var program = astring.generate(
        view.checkParamsExist(ast.identifier('event'), [{name: 'a'}, {name: 'b'}]))
      let event = {$by: 'P', a: "first", b: "second"}
      eval(program).should.be.ok
    })
  })

  describe('checkParamValues', function() {
    it('should check all the parameters', function() {
      var program = astring.generate(
        view.checkParamValues(ast.identifier('event'),
                             {params: [{name: 'a'}, {name: 'b'}], binds:['a', 'b']},
                             ast.identifier('env')))
      let event = {$by: 'P', a: "first", b: "second"}
      let env = {a: "first", b:"second"}
      eval(program).should.be.ok
    })
  })


  describe('eventExpression', function() {
    it('should compile', function() {
      view.eventExpression({
        "type": "Event",
        "by": "patient",
        "name": "Visit",
        "params": [
          {
            "name": "date"
          }
        ],
        "time": null,
        binds: []
      })
    })

    it('should generate code that checks an event for provenance and existing fields', function() {
      var AST = view.eventExpression({
        "type": "Event",
        "by": "patient",
        "name": "Visit",
        "params": [
          {
            "name": "date"
          }
        ],
        "time": null,
        binds: ['date']
      }, ast.identifier('env'))
      var program = astring.generate(AST)
      let env = {patient: 'P', date: "date"}
      let doc = {Visit: {$by: 'P', date: "date"}}
      //log(program)
      eval(program).should.be.ok
    })

    it('should check the time constraints', function() {
      var AST = view.eventExpression({
        "type": "Event",
        "by": "patient",
        "name": "Visit",
        "params": [
          {
            "name": "date"
          }
        ],
        "time": {
          "at": {
            "type": "BinaryExpression",
            "operator": ">",
            "left": {
              "type": "Identifier",
              "name": "t2"
            },
            "right": {
              "type": "Identifier",
              "name": "t"
            }
          }
        },
        binds: ['date', 't2']
      }, ast.identifier('env'))
      //log(JSON.stringify(AST, null, 2))
      var program = astring.generate(AST)
      //log(program)
      let env = {t: 3, patient: 'P', date:'day2', t2: 5}
      let doc = {Visit: {$by: 'P', date: "day2", $time: 5}}
      eval(program).should.be.true
    })
  })

  describe('lookupTimes', function() {
    it('should replace identifiers with env lookups', function() {
      let AST = view.lookupTimes({
        "at": {
          "type": "BinaryExpression",
          "operator": ">",
          "left": {
            "type": "Identifier",
            "name": "t2"
          },
          "right": {
            "type": "Identifier",
            "name": "t"
          }
        }
      }, ast.identifier('env'))
      // log(JSON.stringify(AST))
    })
  })

  describe('translateTimeExpr', function() {
    it('"t2 > t" should eval to true', function() {
      let AST = view.translateTimeExpr(ast.identifier('event'), {
        "at": {
          "type": "BinaryExpression",
          "operator": ">",
          "left": {
            "type": "Identifier",
            "name": "t2"
          },
          "right": {
            "type": "Identifier",
            "name": "t"
          }
        }
      }, ast.identifier('env'))
      // log(JSON.stringify(AST, null, 2))

      let env = {t: 1, t2: 3}
      let program = astring.generate(AST)
      eval(program).should.be.ok
    })
  })

  describe('freeVars', function() {
    it('should return a list of all parameters referenced by a set of events', function() {
      let free = view.freeVars([{
        "type": "Event",
        "by": "P",
        "name": "GrantAccess",
        "params": [
          {
            "name": "recipient"
          },
          {
            "name": "item"
          }

        ],
        "time": null
      },{
        "type": "Event",
        "by": "P",
        "name": "Visit",
        "params": [
          {
            "name": "date"
          }
        ],
        "time": null
      }])

      free.should.eql(['recipient', 'item', 'date'])
    })
  })

  describe('stateExpression', function() {
    it('should handle a single event', function() {
      let AST = view.stateExpression({expression: {
        "type": "Event",
        "by": "doctor",
        "name": "Record",
        "params": [
          {
            "name": "patient"
          },
          {
            "name": "item"
          }
        ],
        "time": null,
        binds: ['patient', 'item']
      }}, ast.identifier('env'))
      // log(JSON.stringify(AST, null, 2))

      let env = {doctor: 'd', item: 'item1', patient: 'p1'}
      let doc = {Record: {$by: 'd', patient: 'p1', item: 'item1'}}
      let program = astring.generate(AST)
      eval(program).should.be.ok
    })

    it('should handle conjunction', function() {
      let AST = view.stateExpression({expression: {
        type: 'BinaryExpression',
        operator: '&&',
        left: {
          "type": "Event",
          "by": "doctor",
          "name": "Record",
          "params": [
            {
              "name": "patient"
            },
            {
              "name": "item"
            }
          ],
          "time": null,
          binds: ['patient', 'item']
        },
        right: {
          "type": "Event",
          "by": "hospital",
          "name": "Store",
          "params": [
            {
              "name": "item"
            }
          ],
          "time": null,
          binds: []
        }}}, ast.identifier('env'))
      // log(JSON.stringify(AST, null, 2))

      let env = {doctor: 'd', hospital: 'h', item:'item1', patient:'p1'}
      let doc = {
        Record: {$by: 'd', patient: 'p1', item: 'item1'},
        Store: {$by: 'h', item: 'item1'}
      }
      let program = astring.generate(AST)
      eval(program).should.be.ok
    })

    it('should handle exception', function() {
      let AST = view.stateExpression({expression: {
        type: 'BinaryExpression',
        operator: 'except',
        left: {
          "type": "Event",
          "by": "doctor",
          "name": "Record",
          "params": [
            {
              "name": "patient"
            },
            {
              "name": "item"
            }
          ],
          "time": null,
          binds: ['patient', 'item']
        },
        right: {
          "type": "Event",
          "by": "hospital",
          "name": "Store",
          "params": [
            {
              "name": "item"
            }
          ],
          "time": null,
          binds: []
        }}}, ast.identifier('env'))
      // log(JSON.stringify(AST, null, 2))

      let env = {doctor: 'd', hospital: 'h', patient:'p1', item:'item1'}
      let doc = {
        Record: {$by: 'd', patient: 'p1', item: 'item1'}
      }
      let program = astring.generate(AST)
      //log(JSON.stringify(program, null, 2))
      eval(program).should.be.ok
    })

    it('should handle "or"', function() {
      return hercule.parseRule('patient.test{a} or doctor.test{b}',
                               'EventConjunction')
        .then(function(parsed) {
          view.assignVars(ast.extractEvents(parsed))
          let AST = view.stateExpression({expression: parsed}, ast.identifier('env'))
          //log(JSON.stringify(AST, null, 2))
          let env = {patient: 'p', doctor: 'd', b:'val'}
          let doc = {
            test: {$by: 'd', b: 'val'}
          }
          let program = astring.generate(AST)
          //log(JSON.stringify(program, null, 2))
          eval(program).should.be.ok
        })
    })
  })

  describe('processNorm', function() {
    it('should handle "created"', function() {
      return hercule.parse('context c(p,d) {commitment thing(p->d): created: p.test{a}}')
        .then(function(context) {
          let states = view.processNorm(context.norms[0])
          // log(JSON.stringify(states, null, 2))
          let AST = states.created

          let env = {p: 'p1', doctor: 'd', a:'val'}
          let doc = {
            test: {$by: 'p1', a: 'val'}
          }
          let program = astring.generate(AST)
          //log(JSON.stringify(program, null, 2))
          eval(program).should.be.ok
        })
    })

    it('should handle "detached"', function() {
      return hercule.parse(
        `context c(p,d) {
           commitment thing(p->d):
             created: p.test{a}
             detached: d.det{a,b}}`)
        .then(function(context) {
          let states = view.processNorm(context.norms[0])
          // log(JSON.stringify(states, null, 2))
          let AST = states.detached
          // log(JSON.stringify(AST, null, 2))

          let env = {p: 'p1', d: 'd1', a:'val', b:2}
          let doc = {
            test: {$by: 'p1', a: 'val'},
            det: {$by: 'd1', a: 'val', b: 2}
          }
          let program = astring.generate(AST)
          // log(JSON.stringify(program, null, 2))
          eval(program).should.be.ok
        })
    })

    it('should handle "discharged"', function() {
      return hercule.parse(
        `context c(p,d) {
           commitment thing(p->d):
             created: p.test{a}
             detached: p.det{a,b}
             discharged: d.dis{b,c}}`)
        .then(function(context) {
          let states = view.processNorm(context.norms[0])
          // log(JSON.stringify(states, null, 2))
          let AST = states.detached
          // log(JSON.stringify(AST, null, 2))

          let env = {p: 'p1', d: 'd1', a:'val', b:2, c:3}
          let doc = {
            test: {$by: 'p1', a: 'val'},
            det: {$by: 'p1', a: 'val', b: 2},
            dis: {$by: 'd1', a: 'val', b: 2, c: 3}
          }
          let program = astring.generate(AST)
          // log(JSON.stringify(program, null, 2))
          eval(program).should.be.ok
        })
    })

    it('should handle "violated"', function() {
      return hercule.parse(
        `context c(p,d) {
           commitment thing(p->d):
             created: p.test{a}
             detached: p.det{a,b}
             discharged: d.dis{b,c}}`)
        .then(function(context) {
          let states = view.processNorm(context.norms[0])
          // log(JSON.stringify(states, null, 2))
          let AST = states.violated
          // log(JSON.stringify(AST, null, 2))

          let env = {p: 'p1', d: 'd1', a:'val', b:2}
          let doc = {
            test: {$by: 'p1', a: 'val'},
            det: {$by: 'p1', a: 'val', b: 2},
          }
          let program = astring.generate(AST)
          // log(JSON.stringify(program, null, 2))
          eval(program).should.be.ok
        })
    })
  })

  describe('processContext', function() {
    it('should produce expressions for each norm', function() {
      return hercule.parse(
        `context c(p,d) {
           commitment thing(p->d):
             created: p.test{a}
             detached: p.det{a,b}
             discharged: d.dis{b,c}}`)
        .then(function(context) {
          context = view.processContext(context)
          Object.keys(context.norms).length.should.equal(1)
        })
    })
  })
})
