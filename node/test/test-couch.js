var assert = require('chai').assert
var should = require('chai').should()

const db = require('couchdb-promises')({
  baseUrl: 'http://admin:password@localhost:5984', // required
  requestTimeout: 10000
})
const dbName = 'testdb'

var log = function() {} //console.log

describe('Array', function() {
  describe('#indexOf()', function() {
    it('should return -1 when the value is not present', function() {
      assert.equal([1,2,3].indexOf(4), -1)
    })
  })
})

describe('Database', function() {
  describe('deleteDatabase', function() {
    it('should delete the database if it exists', function() {
      return db.deleteDatabase(dbName)
    })
  })
  
  describe('createDatabase', function() {
    it('should make a new database', function() {
      return db.createDatabase(dbName)
        .then(log)
    })
  })

  describe('listDatabases', function() {
    it('should list existing databases', function() {
      return db.listDatabases()
        .then(log)
    })
  })

  describe('createDocument', function() {
    it('should create a document', function() {
      return db.createDocument(dbName, {name: 'Bob'})
        .then(log)
    })

    it('should create a document with an ID', function() {
      return db.createDocument(dbName, {name: 'Alice'}, 'alice-id')
        .then(log)
    })

    it('should update a document', function() {
      return db.getDocument(dbName, 'alice-id')
        .then(({data: doc}) => {
          doc.age = 37
          return db.createDocument(dbName, doc, 'alice-id')
        })
        .then(log)
    })
  })

  describe('getDocument', function() {
    it('should retreive a document by id', function() {
      return db.getDocument(dbName, 'alice-id')
        .then(log)
    })
  })

  describe('getAllDocuments', function() {
    it('should retreive all documents in the db', function() {
      return db.getAllDocuments(dbName, {
        descending: true,
        include_docs: true
      }).then(log)
    })
  })

  describe('deleteDocument', function() {
    it('should delete a document', function() {
      return db.getDocument(dbName, 'alice-id')
        .then(({data: {_rev}}) => db.deleteDocument(dbName, 'alice-id', _rev))
        .then(log)
    })
  })

  describe('bulkCreateDocuments', function() {
    it('should create multiple documents at once', function() {
      return db.createBulkDocuments(dbName, [
        {name: 'Tick'}, {name: 'Trick'}, {name: 'Track'}
      ])
        .then(log)
    })
  })

  describe('findDocuments', function() {
    it('should find documents', function() {
      return db.findDocuments(dbName, {
        selector: {
          $or: [{ name: 'Tick' }, {name: 'Track'}]
        },
        fields: ['_id', 'name']
      })
        .then(({data: {docs}}) => {
          docs.should.have.lengthOf(2)
          return docs
        })
        .then(log)
    })
  })
})

const ddoc = {
  language: 'javascript',
  views: { view1: { map: 'function (doc) {emit(doc.name, doc.number)}' } }
}
const docId = 'ddoc1'

describe('Design Documents', function() {
  describe('createDesignDocument', function() {
    it('should create a design document', function() {
      return db.createDesignDocument(dbName, ddoc, docId)
        .then(log)
    })
  })

  describe('getDesignDocument', function() {
    it('should get a design document', function() {
      return db.getDesignDocument(dbName, docId)
        .then(log)
    })
  })

  describe('getView', function() {
    it('should get the documents indexed by a view', function() {
      return db.getView(dbName, docId, 'view1', {limit: 3})
        .then(({data: {rows}}) => {
          rows.should.have.lengthOf(3)
          return rows
        })
        .then(log)
    })
  })
})
