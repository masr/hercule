var assert = require('chai').assert
var should = require('chai').should()
var pegjs = require('pegjs')
var Promise = require('bluebird');
var fs = Promise.promisifyAll(require('fs'));
var rewire = require('rewire')

var hercule = rewire('../hercule')

var parser
describe('Parser', function() {
  it('should load grammar without error', function() {
    return fs.readFileAsync(__dirname + '/../grammar.pegjs', 'utf8')
      .then(pegjs.generate)
      .then(function(obj) {parse = obj.parse})
  })

  it('should parse a simple example', function() {
    return fs.readFileAsync(__dirname + '/../grammar.pegjs', 'utf8')
      .then(pegjs.generate)
      .then(function({parse}) {
        parse('context Test(P) { commitment a(H->P): created: P.test{a} }')
      })
  })

  it('should parse the full Visit spec', function() {
    var parser = fs.readFileAsync(__dirname + '/../grammar.pegjs', 'utf8')
      .then(pegjs.generate)

    var spec = fs.readFileAsync(__dirname + '/../../spec.hcl', 'utf8')

    return Promise.all([parser, spec]).then(function([parser, spec]) {
      try {
        parser.parse(spec)
      }
      catch (e) {
        console.error(e)
        assert.fail()
      }
    })
  })
})

describe('hercule.parse', function() {
  it('should be able to parse the Visit spec', function() {
    return fs.readFileAsync(__dirname + '/../../spec.hcl', 'utf8')
      .then(function(data) {
        return hercule.parse(data)
      })
      .then(function(result) {
        fs.writeFileAsync(__dirname + '/../../parsed-spec.json', JSON.stringify(result, null, 2))
        //console.log(JSON.stringify(result, null, 2))
      })
  })
})
