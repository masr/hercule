var assert = require('chai').assert
var should = require('chai').should()
var acorn = require('acorn')
var rewire = require('rewire');
const loadJsonFile = require('load-json-file')

var hercule = rewire('../hercule')
var Access = hercule.Access

const db = require('couchdb-promises')({
  baseUrl: 'http://admin:password@localhost:5984', // required
  requestTimeout: 10000
})
const dbName = 'testdb'
db.name = dbName

var log = console.log
var testData
describe('Test Data', function() {
  it('should be properly loaded', function() {
    return loadJsonFile(__dirname + '/../../test-data.json').then(json => {
      testData = json
      return db.createBulkDocuments(dbName, json)
    })
  })
})

describe('hercule', function() {
  describe('makeViews', function() {
    it('should generate view functions for all norms in a spec', function() {
      return hercule.parse(
        `context c(p,d) {
           commitment thing(p->d):
             created: p.test{a}
             detached: p.det{a,b}
             discharged: d.dis{b,c}}`)
        .then(function(context) {
          let views = hercule.makeViews(context)
          // log(JSON.stringify(views, null, 2))
          views.thing.should.have.all.keys(['created', 'detached', 'discharged', 'violated'])
        })
    })
  })

  describe('designDocs', function() {
    it('should generate a valid set of design docs', function() {
      return hercule.parse(
        `context c(p,d) {
           commitment thing(p->d):
             created: p.test{a}
             detached: p.det{a,b}
             discharged: d.dis{b,c}}`)
        .then(function(context) {
          let ddocs = hercule.designDocs(context)
          ddocs.should.have.keys(['thing'])
          ddocs.thing.should.have.keys(['language', 'views'])
          ddocs.thing.views.should.have.keys(['created', 'detached', 'discharged', 'violated'])
        })
    })
  })

  describe('loadSpec', function() {
    it('should load a spec and store it in the db as a design document', function() {
      hercule.loadSpec(__dirname + '/../../spec.hcl', db).then(function (docs){
        //console.log(JSON.stringify(docs, null, 2))
      })
    })
  })
})
