var seedrandom =require("seedrandom");
seedrandom("seed", { global: true });
var _ = require('lodash')
var ast = require('./ast')
/*
Objective: generate random contracts and event logs
Purpose: to provide experimental results regarding performance (size, runtime, etc.), as well as automated testing
*/

function predefinedSources(options = {}) {
  "A set of possible roles, event names, params, etc. to work with"
  var {maxRoles=2, maxEventNames=3, maxParams=2,events=[]} = options
  return _.merge(options, {
    maxRoles, maxEventNames, maxParams,
    roles: _.sampleSize([
      "patient",
      "doctor",
      "hospital",
      "nurse",
      "parent",
      "sibling",
      "friend",
      "lab tech",
      "researcher",
      "specialist",
    ], maxRoles),
    eventNames: _.sampleSize([
      "Store",
      "Visit",
      "Record",
      "Store",
      "Retrieve",
      "Access",
      "Share",
      "RequestAccess",
      "RevokeAccess",
    ], maxEventNames),
    params: _.sampleSize([
      "name",
      "age",
      "height",
      "sample",
      "data",
      "item",
      "recipient",
      "date"
    ], maxParams),
    events
  })
}

function generateSources(options = {}) {
  var {maxRoles=2, maxEventNames=3, maxParams=2, events=[]} = options
  return _.merge(options, {
    roles: generateNames('r', maxRoles),
    eventNames: generateNames('e', maxEventNames),
    params: generateNames('p', maxParams),
    events
  })
}

function generateNames(type, n) {
  return _.times(n, i => type + (i+1))
}

function generateEvent({roles, eventNames, params, maxParams=3}) {
  return {
    type: "Event",
    by: _.sample(roles),
    name: _.sample(eventNames),
    params: _.sampleSize(params, _.random(1, maxParams))
  }
}

function generateExpressionTree(options, depth=1) {
  if (options.eventNames && options.eventNames.length>0
      && depth < options.maxDepth && _.random(0,1)) {
    return genBinary(generateExpressionTree(options, depth+1),
                     generateExpressionTree(options, depth+1))
  } else { // leaf node base case
    event = generateEvent(options)
    if (options.events) {
      options.events.push(event)
    } else {
      options.events = [event]
    }

    _.pull(options.eventNames, event.name)
    if (_.random(0,1)) { // 50% chance of event negation (?)
      return ast.except(event)
    } else { // non-negated event
      return event
    }
  }
}

function genBinary(left, right) {
  return {
    type: 'BinaryExpression',
    operator: _.sample(['&&', '||']),
    left, right
  }
}

function generateNorm(options) {
  var norm = {
    type: "Norm",
    kind: _.sample(['commitment', 'authorization', 'prohibition'])
  }

  norm.created = generateExpressionTree(options)
  if (_.random(0,2)) {
    //66% chance of detached event
    norm.detached = generateExpressionTree(options)
    if (_.random(0,2)) {
      //66% chance of detached event
      norm.discharged = generateExpressionTree(options)
    } else {
      if (_.random(0,1)) {
        //50% chance of violated state if not a discharged state
        norm.violated = generateExpressionTree(options)
      }
    }
  }

  return norm
}

function generateContext(options={}) {
  let {name="GeneratedContext", roles, maxNorms = 3} = options
  return {
    name,
    roles: options.roles,
    norms: _.times(_.random(1, maxNorms), () => generateNorm(options)),
    sources: options
  }
}

function getContextEvents(context) {
  var events = []
  for (n of context.norms) {
    for (s of _.keys(_.omit(n, ['type', 'kind']))) {
      events = events.concat(ast.extractEvents(n[s]))
    }
  }
  return _.uniqBy(events, JSON.stringify)
}

function remainingSources(context, enactment) {
  let roles = context.roles
  let eventNames = _.difference(context.sources.eventNames, _.keys(enactment))
  let params = _.difference(context.sources.params, _.keys(enactment))
  return _.merge(context.sources, {roles, eventNames, params, maxParams: params.length})
}

function generateEventInstance(sources) {
  let params = _.sampleSize(sources.params, _.random(1, maxParams))
  let e = {
    "$type": _.sample([sources.eventNames]),
    "$by": _.sample([sources.roles]),
    "$time": _.random(0,1000),
  }
  for (p of params) {
    e[p] = _.random(0,1000)
  }
  return e
}

function generateEnactment() {
  let enactment = {"patient": "P", "doctor": "D", "hospital": "H"}
  let index = _.random(0,9)
  if (index<9) _.assign(enactment, {"Visit": {"$by": "P", "date": "Today"},
                                   "date":"Today"})
  if (index<8) _.assign(enactment, {
    "Record": {"$by":"D", "patient": "P", "item": "EHR"},
    "item": "EHR"})
  if (index<7) _.assign(enactment, {"Store": {"$by":"H", "item": "EHR", "patient": "P"}})
  if (index<6) _.assign(enactment, {"RequestDeletion": {"$by":"P", "item": "EHR"}})
  if (index<5) _.assign(enactment, {"Delete": {"$by":"H", "item": "EHR"}})
  if (index<4) _.assign(enactment, {
    "GrantAccess": {"$by":"P", "recipient": "R", "item": "EHR", "$time": 1},
    "t":1, "recipient":"R", "item":"EHR"})
  if (index<3) {
    let t2 = _.random(2,5)
    _.assign(enactment, {"RequestAccess": {"$by":"R", "item": "EHR", "$time":t2}, t2})
  }
  if (index<2) {
    let t3 = _.random(3,7)
    _.assign(enactment, {"Shared": {"$by":"H", "item": "EHR", "recipient":"R", "$time":t3}})
  }
  return enactment
}


module.exports = {
  generateSources,
  generateEvent,
  generateContext,
  getContextEvents,
  generateEnactment
}
