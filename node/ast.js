var _ = require('lodash')

function traverse(tree, step) {
  var pending = [tree]
  var accum = []

  while (pending.length) {
    [pending, accum] = step(pending, accum)
  }
  return accum
}

function swapNodes(expr, match, swap) {
  let recur = x => swapNodes(x, match, swap)
  if (match(expr)) {
    return swap(expr, recur)
  } else if (expr.left) {
    expr.left = swapNodes(expr.left, match, swap)
    expr.right = swapNodes(expr.right, match, swap)
  } else if (expr.start) {
    expr.start = swapNodes(expr.start, match, swap)
    expr.end = swapNodes(expr.end, match, swap)
  } else if (expr.type == 'UnaryExpression') {
    expr.argument = swapNodes(expr.argument, match, swap)
  } else if (expr.at) {
    expr.at = swapNodes(expr.at, match, swap)
  }
  return expr
}

function stepEvents(pending, accum) {
  curr = pending.pop()
  switch (curr.type) {
  case "Norm":
    pending = pending.concat(_.reverse(curr.states)) //push them all on like a stack
    break
  case "State":
    pending.push(curr.expression)
    break
  case "Event":
    accum.push(curr)
    break
  case "BinaryExpression":
    pending.push(curr.right)
    pending.push(curr.left) //push left second so that it pops first
    break
  case "UnaryExpression":
    pending.push(curr.argument)
    break
  }
  return [pending, accum]
}

function extractEvents(expression) {
  return traverse(expression, stepEvents)
}

function binExp(left, operator, right) {
  return {
    type: 'BinaryExpression',
    left, operator, right
  }
}

function gt(a, b) {
  return binExp(a, '>', b)
}

function lt(a, b) {
  return binExp(a, '<', b)
}

function stepTime(pending, accum) {
  curr = pending.pop()
  if (curr.at) {
    pending.push(curr.at)
  } else if (curr.start) {
    pending.push(curr.end)
    pending.push(curr.start)
  } else if (curr.left) {
    pending.push(curr.right)
    pending.push(curr.left)
  } else if (curr.type == 'Literal') {
    //ignore literal value leaves
  } else if (curr.type == 'Identifier') {
    //reached leaf
    accum.push(curr.name)
  } else {
    console.error(curr)
  }
  return [pending, accum]
}

function extractTimeVars(expr) {
  return traverse(expr, stepTime)
}

function assign(place, value) {
  return {
    type: "AssignmentExpression",
    operator: '=',
    left: place,
    right: value
  }
}

function cond(test, consequent, alternate) {
  return {
    type: "ConditionalExpression",
    test, consequent, alternate
  }
}

function equal(a, b) {
  return {
    type:"BinaryExpression",
    operator: "==",
    left: a,
    right: b
  }
}

function func(params, body) {
  "body should be an array of directives or statements"
  return {
    type: "FunctionExpression",
    params,
    body: {
      type: 'BlockStatement',
      body: body
    }
  }
}

function ret(expr) {
  return {
    type: "ReturnStatement",
    argument: expr
  }
}

function staticMember(object, property) {
  return {
    type: "MemberExpression",
    object, property,
    computed: false
  }
}

function dynamicMember(object, property) {
  return {
    type: "MemberExpression",
    object, property,
    computed: true
  }
}

function identifier(name) {
  return {
    type: "Identifier",
    name
  }
}

function literal(value) {
  return {
    type: "Literal",
    value
  }
}

function and(...expressions) {
  expressions = _.filter(expressions)
  if (expressions.length == 1) {
    return expressions[0]
  } else {
    return {
      type: "LogicalExpression",
      operator: "&&",
      left: expressions.shift(),
      right: expressions.length > 1 ? and(...expressions) : expressions[0]
    }
  }
}

function or(...expressions) {
  if (expressions.length == 1) {
    return expressions[0]
  } else {
    return {
      type: "LogicalExpression",
      operator: "||",
      left: expressions.shift(),
      right: expressions.length > 1 ? or(...expressions) : expressions[0]
    }
  }
}

function not(expr) {
  return {
    type: 'UnaryExpression',
    operator: '!',
    prefix: true,
    argument: expr
  }
}

function except(expr) {
  return {
    type: 'UnaryExpression',
    operator: 'except',
    prefix: true,
    argument: expr
  }
}

function filter(seq, fn) {
  return call(
    staticMember(seq, identifier('filter')),
    [ fn ]
  )
}

function call(callee, arguments) {
  return {
    type: "CallExpression",
    callee,
    arguments
  }
}

module.exports = {
  traverse,
  swapNodes,
  stepEvents,
  extractEvents,
  binExp,
  gt,
  lt,
  stepTime,
  extractTimeVars,
  assign,
  cond,
  equal,
  func,
  ret,
  staticMember,
  dynamicMember,
  identifier,
  literal,
  and,
  or,
  not,
  except,
  filter,
  call
}
