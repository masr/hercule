var _ = require('lodash')
var ast = require('./ast')

function consoleLog(value) {
  return ast.call(ast.staticMember(ast.identifier('console'), ast.identifier('log')),
                  [value])
}

function processContext(context, env=ast.identifier('env')) {
  var norms = {}
  context.norms.forEach(norm => {
    norms[norm.name] = processNorm(norm, env)
  })
  context.norms = subReferences(norms)
  return context
}

function subReferences(norms) {
  return _.mapValues(norms, function(norm) {
    return _.mapValues(norm, function(state) {
      return ast.swapNodes(state,
                           l => l.type == 'Reference',
                           l => norms[l.name][l.state])
    })
  })
}

function processNorm(norm, env=ast.identifier('env')) {
  let events = ast.extractEvents(norm)
  assignVars(events)

  var states = {}
  norm.states.forEach(function(s) {
    states[s.name] = stateExpression(s, env)
  })

  if (states.detached)
    states.detached = ast.and(states.created, states.detached)

  switch (norm.kind) {
  case 'commitment':
    if (states.discharged) {
      states.discharged = ast.or(ast.and(states.created, states.discharged),
                                 ast.and(states.detached, states.discharged))
    }
    if (states.violated) {
      states.violated = ast.and(states.detached, states.violated)
    } else {
      states.violated = ast.and(states.detached, ast.not(states.discharged))
    }
    break;
  case 'authorization':
    if (states.discharged && states.detached) {
      states.violated = ast.and(states.detached, ast.not(states.discharged))
      states.discharged = ast.and(states.detached, states.discharged)
    }
    break
  case 'prohibition':
    if (states.detached)
      states.discharged = ast.and(states.detached, ast.not(states.violated))
    break;
  }

  return states
}

function freeVars(events) {
  let vars = _.flatMap(events, e => e.params).map(p => p.name)
  let timeVars = _.flatMap(_.filter(events.map(e => e.time)), x => ast.extractTimeVars(x))
  return _.uniq(vars.concat(timeVars))
}

function assignVars(events) {
  let free = freeVars(events)
  events.forEach(function(e) {
    var binding = _.intersection(freeVars([e]), free)
    e.binds = binding
    _.pullAll(free, binding) //remove bound variables from array
  })
}

function stateExpression(state, env) {
  expr = ast.swapNodes(state.expression,
                       l => (l.operator == 'except'),
                       (l, recur) => {
                         l.operator = '&&'
                         l.left = recur(l.left)
                         l.right = ast.not(recur(l.right))
                         return l
                       })
  expr = ast.swapNodes(expr,
                       l => l.type == "Event",
                       l => eventExpression(l, env))
  return expr
}


function eventExpression(event, env) {
  let d = ast.identifier('doc')
  let e = ast.staticMember(d, ast.identifier(event.name))
  let expr = ast.and(
    e,
    checkBy(e, ast.staticMember(env, ast.identifier(event.by))),
    checkParamsExist(e, event.params),
    checkParamValues(e, event, env),
    checkTime(e, event, env)
  )
  return ast.swapNodes(expr,
                       l => l.at,
                       (l, recur) => recur(l.at))
}

function checkBy(obj, value) {
  return ast.equal(ast.staticMember(obj, ast.identifier('$by')), value)
}

function checkParamsExist(obj, params) {
  return ast.and(...params.map(function(p) {
    return ast.staticMember(obj, ast.identifier(p.name))
  }))
}

function checkParamValues(obj, event, env) {
  return ast.and(...event.params.map(function(p) {
    let s = ast.staticMember(env, ast.identifier(p.match || p.name))
    let v = ast.staticMember(obj, ast.identifier(p.name))
    return ast.equal(s, v)
  }))
}

function checkTime(obj, event, env) {
  expr = event.time
  if (!expr) return
  let timeVar = _.intersection(event.binds, ast.extractTimeVars(expr))[0]
  expr = translateTimeExpr(obj, expr, env)
  if (timeVar) {
    expr = ast.swapNodes(expr,
                         expr => expr.type == 'Identifier' && expr.name == timeVar,
                         expr => ast.staticMember(obj, ast.identifier('$time')))
  }
  return expr
}

function lookupTimes(expr, env) {
  return ast.swapNodes(expr,
                       expr => expr.type == 'Identifier', // leaf
                       expr => ast.staticMember(env, ast.identifier(expr.name))) // swap
}

function translateTimeExpr(obj, expr, env) {
  expr = lookupTimes(expr, env)
  let $time = ast.staticMember(obj, ast.identifier('$time'))
  if (expr.at) {
    if (expr.at.type == 'Identifier') {
      return ast.staticMember(env, expr.at)
    } else if (['>', '<', '=='].includes(expr.at.operator)) {
      return expr.at
    } else {
      return ast.equal($time, expr)
    }
  } else if (expr.start) {
    return ast.and(ast.gt($time, expr.start),
                   ast.lt($time, expr.end))
  }
}

module.exports = {
  consoleLog,
  processContext,
  subReferences,
  processNorm,
  freeVars,
  assignVars,
  stateExpression,
  eventExpression,
  checkBy,
  checkParamsExist,
  checkParamValues,
  checkTime,
  lookupTimes,
  translateTimeExpr
}
