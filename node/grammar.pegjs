Context
 = 'context' _ name:String '(' roles:NameList ')' _ '{' norms:Norm+ _ '}' { return {name, roles, norms} }

NameList
 = first:String ',' _ rest:NameList { return [first].concat(rest) }
 / name:String {return [name]}

Norm
  = _ kind:String _ name:String '(' relationship:Relationship ','? _ params:ParamList? ')' ':'? _ states:State+ { return {type:'Norm', kind, name, relationship, params, states} }

Relationship
 = from:String '->' to:String {return {from, to}}
 
State
 = _ name:String ':'? _ expression:EventConjunction { return {type:'State', name, expression} }

EventConjunction
 = left:EventDisjunction _ op:('and' / 'except') _ right:EventConjunction { return {type: 'BinaryExpression', operator: op == 'and' ? '&&' : 'except', left, right}}
 / EventDisjunction

EventDisjunction
 = left:EventUnary _ 'or' _ right:EventDisjunction {
     return {type: 'BinaryExpression', operator: '||', left, right}
   }
 / EventUnary

EventUnary
 = op:('not' / 'unless') _ expr:EventClause { return {type: 'UnaryExpression', operator: '!', argument: expr} }
 / EventClause

EventClause
 = '(' _ expr:EventConjunction _ ')' { return expr }
 / Event
 / Reference
 
Event
 = role:String '.' name:String '{' params:ParamList '}' time:TimeClause? { return {type: 'Event', by: role, name, params, time} }

Reference
  = name:String '(' relationship:Relationship ','? _ params:ParamList? ')' ':' state:String {
    return {name, type:'Reference', relationship, params, state}
  }

ParamList
 = first:Param ','? _? rest:ParamList?
 { return rest ? [first].concat(rest) : [first] }

Param
 = name:String match:Match? subset:In?
 {
   var obj = {name}
   if (match) obj.match = match
   if (subset) obj.subset = subset
   return obj
 }
 
Match
 = ':' _ name:String
 {return name}
 
In
 = _ 'in' _ set:String
 {return set }

TimeClause
 = _ '@' _ at:TimeComp {return {at}}
 / _ '@' _ '[' _ start:TimeExpr _ ',' _ end:TimeExpr _ ']' { return {start, end} }

TimeComp
 = left:TimeExpr _ operator:('>' / '<' / '==') _ right:TimeComp {
     return {type: 'BinaryExpression', operator, left, right}
   }
 / TimeExpr

TimeExpr
 = left:TimeTerm _ operator:('+' / '-') _ right:TimeExpr {
     return {type: 'BinaryExpression', operator, left, right}
   }
 / TimeTerm

TimeTerm
 = left:TimeFactor _ operator:('*' / '/') _ right:TimeTerm {
   return {type: 'BinaryExpression', operator, left, right}
 }
 / TimeFactor

TimeFactor
 = '(' _ TimeExpr _ ')'
 / Time

Time
 = Integer
 / name:String { return {type: "Identifier", name} }

Integer "integer"
 = _ [0-9]+ { return {type: "Literal", value: parseInt(text(), 10)}}

String
 = [a-zA-Z0-9]+ { return text() }
 
_ "whitespace"
 = [ \t\n\r]*