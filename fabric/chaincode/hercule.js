/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');

class Hercule extends Contract{
    async initLedger(ctx){
      console.info("============= Initializing Empty Ledger ===========")
    }
    async queryEnactment(ctx,eventNumber){
        const evntAsBytes = await ctx.stub.getState(eventNumber);
        if (!evntAsBytes|| evntAsBytes.length===0){
            throw new Error(`${eventNumber} does not exist`);
        }
        console.log(evntAsBytes.toString());
        return evntAsBytes.toString();
    }
    async existsEnactment(ctx,eventNumber){
        const evntAsBytes = await ctx.stub.getState("ENACTMENT_"+eventNumber);
        if (!evntAsBytes|| evntAsBytes.length===0){
            return(false);
        }
        return(true);
    }

    async createEnactmentFromStrWithRandomID(ctx,tx_str){
        var tx=JSON.parse(tx_str);
        var enactment_no=this.createEnactment(ctx,tx);
        return(enactment_no);
    }
    async createEnactment(ctx,tx){
        console.log('============= START : Create Event ===========');
        var added=False;
        var timeout=10
        while (!added && timeout>0){
            --timeout;
            var enactmentID=Math.floor(Math.random()*100);
            const enactmentAsBytes = await ctx.stub.getState("ENACTMENT_"+enactmentID);
            if (!enactmentAsBytes || enactmentAsBytes.length === 0) {
                console.log(enactmentID,tx);
                await ctx.stub.putState("ENACTMENT_"+enactmentID, Buffer.from(JSON.stringify(tx)));
                added=True
                console.info('============= END : Create Event ===========');
                return(enactmentID);
            }
        }
    }
    async createEnactmentFromStr(ctx,enactment_no,tx_str){
        var tx=JSON.parse(tx_str);
        var enactment_no = await this.createEnactment(ctx,tx,enactment_no);
        return(enactment_no);
    }
    async createEnactment(ctx,tx,enactmentID){
        console.log('============= START : Create Enactment ===========');
        await ctx.stub.putState("ENACTMENT_"+enactmentID, Buffer.from(JSON.stringify(tx)));
        console.log("ENACTMENT_"+enactmentID,tx);
        console.info('============= END : Create Enactment ===========');
        return(enactmentID);
    }

    async queryAllEnactments(ctx) {
        const startKey = 'ENACTMENT_0';
        const endKey = 'ENACTMENT_999';

        const iterator = await ctx.stub.getStateByRange(startKey, endKey);

        const allResults = [];
        while (true) {
            const res = await iterator.next();

            if (res.value && res.value.value.toString()) {
                console.log(res.value.value.toString('utf8'));

                const Key = res.value.key;
                let Record;
                try {
                    Record = JSON.parse(res.value.value.toString('utf8'));
                } catch (err) {
                    console.log(err);
                    Record = res.value.value.toString('utf8');
                }
                allResults.push({ Key, Record });
            }
            if (res.done) {
                console.log('end of data');
                await iterator.close();
                console.info(allResults);
                return JSON.stringify(allResults);
            }
        }
    }

    async addEventToEnactment(ctx,enact_no,eventType,eventData){
        console.info('============= START : add event to existing enactment ===========');

        const enactmentAsBytes = await ctx.stub.getState("ENACTMENT_"+enact_no); // get the car from chaincode state
        if (!enactmentAsBytes || enactmentAsBytes.length === 0) {
            throw new Error(`${enact_no} does not exist`);
        }
        const enactment = JSON.parse(enactmentAsBytes.toString());
        console.log(enactment);
        const event = JSON.parse(eventData);
        enactment[eventType] = event

        await ctx.stub.putState("ENACTMENT_"+enact_no, Buffer.from(JSON.stringify(enactment)));
        console.info('============= END : added Event to Enactment ===========');
        return enact_no;
    }
}

module.exports.Hercule = Hercule;
module.exports.contracts = [ Hercule ];
