# hercule-fabric

Blockchain platform for integration with the Hercule norm reasoning system

# Setup

Run `./startFabric.sh` and wait for the containers to initalize and instantiate the chaincode.

# Interacting with Hercule via Fabric chaincode

Use `client.js` in `hercule-client` to query and submit transactions

1. Update the `tx.json` file to specify enactments
2. `node client.js tx ./tx.json` will submit the enactments to the ledger
3. `node client.js getEnactment 1` will get data in enactment 1 from the ledger
4. `node client.js queryAll` to query all enactments

## Notes
Implemented by modifying fabcar module from fabric-samples and consolidating dependecies for running fabric.
