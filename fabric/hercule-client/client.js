/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const program = require('commander');
const fs = require('fs');
const { FileSystemWallet, Gateway } = require('fabric-network');
const path = require('path');
const ccpPath = path.resolve(__dirname, '..', 'network', 'connection-org1.json');

function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
      if ((new Date().getTime() - start) > milliseconds){
        break;
      }
    }
}
function read_json_tx(fpath){
    try{
        var tx=JSON.parse(fs.readFileSync(fpath));
        return tx;
    }
    catch(error){
        console.log(error);
    }
}
async function submitToHercule(path){
    try {
        var json=read_json_tx(path);
        console.log(json);
        for(var ev in json["Events"]){
            console.log("----------------------PROCESSING EVENT:----------------------")
            var enactment_id=Object.keys(json["Events"][ev])[0];
            var eventjson=json["Events"][ev][enactment_id];
            var event_type=Object.keys(eventjson)[0];
            var event_data=eventjson[event_type];
            var exists=await enactmentExists(enactment_id);

            if (!exists){
                console.log("-----Creating Enactment-----");
                var created= await createEnactment(enactment_id,JSON.stringify(json["Enactments"][enactment_id]));
                sleep(5000);
                if (created){
                    exists=true;
                }
            }
            else{
                console.log("Enactment ID:",enactment_id,"exists");
            }
            if (exists){
                console.log("-----Adding Event-----");
                var addEvent = await submitEventToHercule(enactment_id,event_type,JSON.stringify(event_data))
                sleep(5000);
            }
            sleep(5000);
        }
    } catch (error) {
        console.log(error);
    }
}
async function createEnactment(enactment_no,enactment_str){
    try {
        // Create a new file system based wallet for managing identities.
        const walletPath = path.join(process.cwd(), 'wallet');
        const wallet = new FileSystemWallet(walletPath);
        console.log(`Wallet path: ${walletPath}`);

        // Check to see if we've already enrolled the user.
        const userExists = await wallet.exists('user1');
        if (!userExists) {
            console.log('An identity for the user "user1" does not exist in the wallet');
            console.log('Run the registerUser.js application before retrying');
            return;
        }

        // Create a new gateway for connecting to our peer node.
        const gateway = new Gateway();
        await gateway.connect(ccpPath, { wallet, identity: 'user1', discovery: { enabled: true, asLocalhost: true } });

        // Get the network (channel) our contract is deployed to.
        const network = await gateway.getNetwork('mychannel');

        // Get the contract from the network.
        const contract = network.getContract('hercule');
        console.log('============= START : Submit Transaction ===========');
        // Evaluate the specified transaction.
        // queryCar transaction - requires 1 argument, ex: ('queryCar', 'CAR4')
        // queryAllCars transaction - requires no arguments, ex: ('queryAllCars')
        var result= await contract.submitTransaction('createEnactmentFromStr',enactment_no,enactment_str);
        result=result.toString();
        console.log('Transaction has been submitted');
        console.log('============= End : Submit Transaction ===========');
        gateway.disconnect();
        if(result==enactment_no){
            console.log(`Enactment${enactment_no} Created`)
            return true;
        }
        return false;


    } catch (error) {
        console.error(`Failed to submit transaction: ${error}`);
        process.exit(1);
    }
}
async function submitEventToHercule(enactment_no,eventType,eventData){
    try {
        // Create a new file system based wallet for managing identities.
        const walletPath = path.join(process.cwd(), 'wallet');
        const wallet = new FileSystemWallet(walletPath);
        // console.log(`Wallet path: ${walletPath}`);

        // Check to see if we've already enrolled the user.
        const userExists = await wallet.exists('user1');
        if (!userExists) {
            console.log('An identity for the user "user1" does not exist in the wallet');
            console.log('Run the registerUser.js application before retrying');
            return;
        }

        // Create a new gateway for connecting to our peer node.
        const gateway = new Gateway();
        await gateway.connect(ccpPath, { wallet, identity: 'user1', discovery: { enabled: true, asLocalhost: true } });

        // Get the network (channel) our contract is deployed to.
        const network = await gateway.getNetwork('mychannel');

        // Get the contract from the network.
        const contract = network.getContract('hercule');
        console.log('============= START : Submit Transaction ===========');
        // Evaluate the specified transaction.
        // queryCar transaction - requires 1 argument, ex: ('queryCar', 'CAR4')
        // queryAllCars transaction - requires no arguments, ex: ('queryAllCars')
        var result= await contract.submitTransaction('addEventToEnactment',enactment_no,eventType,eventData);
        console.log('Transaction has been submitted');
        console.log('============= End : Submit Transaction ===========');
        gateway.disconnect();

    } catch (error) {
        console.error(`Failed to submit transaction: ${error}`);
        process.exit(1);
    }
}
async function enactmentExists(enactment_id){
    try {
        // Create a new file system based wallet for managing identities.
        const walletPath = path.join(process.cwd(), 'wallet');
        const wallet = new FileSystemWallet(walletPath);
        //  console.log(`Wallet path: ${walletPath}`);

        // Check to see if we've already enrolled the user.
        const userExists = await wallet.exists('user1');
        if (!userExists) {
            console.log('An identity for the user "user1" does not exist in the wallet');
            console.log('Run the registerUser.js application before retrying');
            return;
        }

        // Create a new gateway for connecting to our peer node.
        const gateway = new Gateway();
        await gateway.connect(ccpPath, { wallet, identity: 'user1', discovery: { enabled: true, asLocalhost: true } });

        // Get the network (channel) our contract is deployed to.
        const network = await gateway.getNetwork('mychannel');

        // Get the contract from the network.
        const contract = network.getContract('hercule');

        // Evaluate the specified transaction.
        // queryCar transaction - requires 1 argument, ex: ('queryCar', 'CAR4')
        // queryAllCars transaction - requires no arguments, ex: ('queryAllCars')
        const result = await contract.evaluateTransaction('existsEnactment',enactment_id);
        //  console.log(`Transaction has been evaluated, result is: ${result.toString()}`);
        return(result.toString()=='true');
    } catch (error) {
        console.error(`Failed to evaluate transaction: ${error}`);
        process.exit(1);
    }
}
async function queryAllHerculeEnactments() {
    try {

        // Create a new file system based wallet for managing identities.
        const walletPath = path.join(process.cwd(), 'wallet');
        const wallet = new FileSystemWallet(walletPath);
        // console.log(`Wallet path: ${walletPath}`);

        // Check to see if we've already enrolled the user.
        const userExists = await wallet.exists('user1');
        if (!userExists) {
            console.log('An identity for the user "user1" does not exist in the wallet');
            console.log('Run the registerUser.js application before retrying');
            return;
        }

        // Create a new gateway for connecting to our peer node.
        const gateway = new Gateway();
        await gateway.connect(ccpPath, { wallet, identity: 'user1', discovery: { enabled: true, asLocalhost: true } });

        // Get the network (channel) our contract is deployed to.
        const network = await gateway.getNetwork('mychannel');

        // Get the contract from the network.
        const contract = network.getContract('hercule');

        // Evaluate the specified transaction.
        // queryCar transaction - requires 1 argument, ex: ('queryCar', 'CAR4')
        // queryAllCars transaction - requires no arguments, ex: ('queryAllCars')
        const result = await contract.evaluateTransaction('queryAllEnactments');
        console.log(`Transaction has been evaluated, result is: ${result.toString()}`);
        gateway.disconnect();

    } catch (error) {
        console.error(`Failed to evaluate transaction: ${error}`);
        process.exit(1);
    }
}
async function queryEnactment(enactment_id){
    try {

        // Create a new file system based wallet for managing identities.
        const walletPath = path.join(process.cwd(), 'wallet');
        const wallet = new FileSystemWallet(walletPath);
        // console.log(`Wallet path: ${walletPath}`);

        // Check to see if we've already enrolled the user.
        const userExists = await wallet.exists('user1');
        if (!userExists) {
            console.log('An identity for the user "user1" does not exist in the wallet');
            console.log('Run the registerUser.js application before retrying');
            return;
        }

        // Create a new gateway for connecting to our peer node.
        const gateway = new Gateway();
        await gateway.connect(ccpPath, { wallet, identity: 'user1', discovery: { enabled: true, asLocalhost: true } });

        // Get the network (channel) our contract is deployed to.
        const network = await gateway.getNetwork('mychannel');

        // Get the contract from the network.
        const contract = network.getContract('hercule');

        // Evaluate the specified transaction.
        // queryCar transaction - requires 1 argument, ex: ('queryCar', 'CAR4')
        // queryAllCars transaction - requires no arguments, ex: ('queryAllCars')
        const result = await contract.evaluateTransaction('queryEnactment',"ENACTMENT_"+enactment_id);
        console.log(`Transaction has been evaluated, result is: ${result.toString()}`);
        gateway.disconnect();

    } catch (error) {
        console.error(`Failed to evaluate transaction: ${error}`);
        process.exit(1);
    }
}

program
.command('queryAll')
.description('Queries all Hercule Events in the Ledger')
.action(function(){
    console.log("Querying");
    queryAllHerculeEnactments();
})

program
.command('tx <path>')
.description('The json file provided as the argument will be added as a tx to ledger')
.action(function(path){
    submitToHercule(path);
})

program
.command("exists <id>")
.action(function(id){
    enactmentExists(id).then(console.log);
})

program
.command("getEnactment <id>")
.action(function(id){
    queryEnactment(id);
})

program.parse(process.argv); // end with parse to parse through the input0
